import * as AwsMock from 'aws-sdk-mock';
import { config } from 'dotenv';

config({ path: './.env.testing' });
jest.setTimeout(30 * 1000);
jest.retryTimes(3);

beforeAll(() => {
  AwsMock.mock('SNS', 'publish', (params, callback) =>
    callback(undefined, 'success'),
  );
});

afterAll(() => {
  AwsMock.restore();
});

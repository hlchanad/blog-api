FROM amazonlinux:2

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY

WORKDIR /app

RUN yum -y install curl && \
    curl -sL https://rpm.nodesource.com/setup_14.x | bash - && \
    yum install -y nodejs gcc-c++ make

COPY ./package* ./

RUN npm i

RUN ./node_modules/.bin/serverless config credentials --provider aws --key $AWS_ACCESS_KEY_ID --secret $AWS_SECRET_ACCESS_KEY

COPY . .

CMD ["npm", "run", "deploy:dev"]

/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.json',
      isolatedModules: true,
      diagnostics: false,
    },
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  testRegex: '\\.spec\\.ts$',
  transform: {
    '^.+\\.(j|t)s$': 'ts-jest',
  },
  testPathIgnorePatterns: ['/node_modules/', '/lib/'],
  setupFilesAfterEnv: ['jest-extended', './jest.setup.ts'],
  moduleNameMapper: {
    '^@helpers$': '<rootDir>/src/helpers',
    '^@database$': '<rootDir>/src/database',
    '^@test$': '<rootDir>/src/test',
    '^@modules/(.*)$': '<rootDir>/src/modules/$1',
  },
};

import { database } from '@database';
import { parseAuthHeader } from '@modules/auth';
import { LoginService } from '@modules/user';
import { ApolloServer } from 'apollo-server-lambda';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { MongooseDataloaderFactory } from 'graphql-dataloader-mongoose';
import { schema } from './schema';
import { Context } from './types';

const server = new ApolloServer({
  schema,
  context: async ({ event }): Promise<Context> => ({
    user: await parseAuthHeader(
      LoginService.parseToken,
      event.headers.Authorization,
    ),
    dataloader: new MongooseDataloaderFactory(),
  }),
  plugins: [
    ApolloServerPluginLandingPageGraphQLPlayground({
      settings: {
        'schema.polling.enable': false,
      },
    }),
  ],
});

export const graphqlHandler = server.createHandler();

export const handler = async function (event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;
  await database.mongo.connect();
  return graphqlHandler(event, context, callback);
};

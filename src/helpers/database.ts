import { pluralize } from 'graphql-compose';
import { snakeCase } from 'lodash';

export function getCollectionName(module: string, collection: string): string {
  return `${snakeCase(module)}__${pluralize(snakeCase(collection))}`;
}

export function appendText(
  original: string,
  extend: string,
  delimiter = '. ',
): string {
  return original.endsWith(delimiter)
    ? `${original}${extend}`
    : `${original}${delimiter}${extend}`;
}

export function summaryOf(text: string, max = 100): string {
  if (text.length <= max) {
    return text;
  }
  return text.substring(0, text.lastIndexOf(' ', max));
}

import { appendText, summaryOf } from '../text';

describe('appendText()', () => {
  it('returns table name', async () => {
    expect(appendText('hello', 'peter')).toBe('hello. peter');
    expect(appendText('hello. ', 'peter')).toBe('hello. peter');
  });
});

describe('summaryOf()', () => {
  it('returns whole text of short text', async () => {
    const text = 'This is a short text.';
    const result = summaryOf(text);
    expect(result).toBe(text);
  });

  it('returns summary of short text', async () => {
    const text = 'This is a extremely super long long text.';
    const result = summaryOf(text, 30);
    expect(result).toBe('This is a extremely super long');
  });
});

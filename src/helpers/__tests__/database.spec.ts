import { getCollectionName } from '../database';

describe('getCollectionName()', () => {
  it('returns table name', async () => {
    expect(getCollectionName('A', 'ApplePen')).toBe('a__apple_pens');
    expect(getCollectionName('AbCd', 'ApplePen')).toBe('ab_cd__apple_pens');
    expect(getCollectionName('Ab_Cd', 'ApplePen')).toBe('ab_cd__apple_pens');

    expect(getCollectionName('A', 'apple_pen')).toBe('a__apple_pens');
    expect(getCollectionName('AbCd', 'apple_pen')).toBe('ab_cd__apple_pens');
    expect(getCollectionName('Ab_Cd', 'apple_pen')).toBe('ab_cd__apple_pens');
  });
});

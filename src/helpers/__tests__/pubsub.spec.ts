import { parseEvent } from '../pubsub';
import { mockEvent } from '@test';

describe('parseEvent()', () => {
  it('parses event', async () => {
    const result = parseEvent(mockEvent({ hello: 'world' }));
    expect(result).toMatchObject({ hello: 'world' });
  });
});

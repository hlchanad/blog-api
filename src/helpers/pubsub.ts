import { SNS } from 'aws-sdk';
import { config } from './config';

let sns: SNS;

function getSNS() {
  if (!sns) {
    sns = new SNS({
      region: config('aws.region'),
    });
  }

  return sns;
}

export function publish(topic: string, payload: any) {
  return getSNS()
    .publish({
      Message: JSON.stringify(payload),
      TopicArn: topic,
    })
    .promise();
}

export function parseEvent<T>(event): T {
  const snsEvent = JSON.parse(event.Records[0].body);
  return JSON.parse(snsEvent.Message) as unknown as T;
}

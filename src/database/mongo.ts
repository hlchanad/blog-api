import * as mongoose from 'mongoose';

import { config } from '@helpers';

let isConnected = false;

function getConnectionString(): string {
  const { host, port, user, password, database, uri } =
    config('database.mongo');
  return uri ?? `mongodb://${user}:${password}@${host}:${port}/${database}`;
}

async function _connect(
  uri: string = getConnectionString(),
  options: mongoose.ConnectOptions = {},
): Promise<mongoose.Connection> {
  const DEFAULT_OPTIONS = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };

  options = Object.assign({}, DEFAULT_OPTIONS, options);

  const startTime = new Date().getTime();

  const { connection } = await mongoose.connect(uri, options).catch((error) => {
    console.error('failed to connect database', error);
    throw error;
  });

  const endTime = new Date().getTime();

  isConnected = true;

  console.info(`connected database, used ${endTime - startTime}ms`);

  return connection;
}

export async function connect(
  uri: string = getConnectionString(),
  options: mongoose.ConnectOptions = {},
): Promise<mongoose.Connection> {
  if (isConnected) {
    console.info('using existing database connection');
    return mongoose.connection;
  }

  return _connect(uri, options);
}

export async function close() {
  await mongoose.connection.close();
}

import { Document } from 'mongoose';

export async function parseAuthHeader<TUser extends Document>(
  parseToken: (token: string) => Promise<TUser>,
  authorization: string,
): Promise<TUser> {
  if (!authorization || authorization.indexOf('Bearer') < 0) {
    return null;
  }

  const token = authorization.split(' ')[1];
  return parseToken(token);
}

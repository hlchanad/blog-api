import { UserSeed } from '@modules/user/models/user.seed';
import { MongoInMemory } from '@test';
import {
  Resolver,
  ResolverResolveParams,
  schemaComposer,
} from 'graphql-compose';
import * as mongoose from 'mongoose';
import { requireLogin, requireLoginResolver } from '../require-login';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('requireLoginResolver()', () => {
  it('throws if user is absent in context', () => {
    const next = jest.fn();
    const resolverParams = {
      context: {},
    } as ResolverResolveParams<any, any>;

    expect(() => requireLoginResolver()(next)(resolverParams)).toThrow(
      'not authenticated',
    );
    expect(next).not.toBeCalled();
  });

  it('throws if user does not have required scopes', async () => {
    const next = jest.fn();
    const resolverParams = {
      context: {
        user: await UserSeed({ scopes: [{ scope: 'read' }] }),
      },
    } as ResolverResolveParams<any, any>;

    expect(() => requireLoginResolver(['write'])(next)(resolverParams)).toThrow(
      'not authorized',
    );
    expect(next).not.toBeCalled();
  });

  it('calls the next resolver function if user has required scopes', async () => {
    const next = jest.fn();
    const resolverParams = {
      context: {
        user: await UserSeed({
          scopes: [{ scope: 'read' }, { scope: 'write' }],
        }),
      },
    } as ResolverResolveParams<any, any>;

    requireLoginResolver(['write'])(next)(resolverParams);
    expect(next).toBeCalled();
  });

  it('calls the next resolver function if user is super user', async () => {
    const next = jest.fn();
    const resolverParams = {
      context: {
        user: await UserSeed({
          isSuperUser: true,
        }),
      },
    } as ResolverResolveParams<any, any>;

    requireLoginResolver(['write'])(next)(resolverParams);
    expect(next).toBeCalled();
  });
});

describe('requireLogin()', () => {
  let resolver: Resolver;

  beforeEach(async () => {
    resolver = schemaComposer.createResolver({
      type: 'String!',
      name: 'test',
      kind: 'mutation',
      description: 'Test description',
      resolve: async () => {
        return 'Hello World';
      },
    });
  });

  it('appends description if scopes provided', async () => {
    const newResolver = resolver.wrap(requireLogin(['scope1', 'scope2']));

    expect(newResolver.description).toBe(
      'Test description. Required scopes: scope1, scope2',
    );
  });

  it('does nothing on description if scopes absent', async () => {
    const newResolver = resolver.wrap(requireLogin());

    expect(newResolver.description).toBe('Test description');
  });

  it('wraps with requireLoginResolver()', async () => {
    const fn = jest.fn().mockReturnValue((next) => (rp) => next(rp));

    resolver.wrap(requireLogin(['scope1', 'scope2'], fn));

    expect(fn).toBeCalled();
    expect(fn).toBeCalledWith(['scope1', 'scope2']);
  });
});

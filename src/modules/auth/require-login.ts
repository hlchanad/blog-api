import { appendText } from '@helpers';
import { NotAuthenticatedError, NotAuthorizedError } from '@modules/graphql';
import { ResolverNextRpCb, ResolverWrapCb } from 'graphql-compose';

export function requireLoginResolver<
  T extends {
    user?: {
      isSuperUser: boolean;
      hasScopes: (scopes: string[]) => boolean;
    };
  },
>(scopes?: string[]): ResolverNextRpCb<any, T> {
  return (next) => (rp) => {
    const user = rp.context.user;

    if (!user) {
      throw new NotAuthenticatedError();
    }

    if (scopes.length && !user.isSuperUser && !user.hasScopes(scopes)) {
      throw new NotAuthorizedError();
    }

    return next(rp);
  };
}

export function requireLogin(
  scopes?: string[],
  wrapResolve = requireLoginResolver,
): ResolverWrapCb<any, any, any> {
  return (resolver) => {
    const description = scopes?.length
      ? appendText(
          resolver.description,
          `Required scopes: ${scopes.join(', ')}`,
        )
      : resolver.description;

    return resolver
      .wrapResolve(wrapResolve(scopes))
      .setDescription(description);
  };
}

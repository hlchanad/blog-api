import * as faker from 'faker';
import { Category, CategoryModel } from './category.model';

export async function CategorySeed(
  args?: Partial<Category>,
): Promise<Category> {
  const tags = await CategoryModel.create(
    [
      {
        name: faker.lorem.word(),
        createdAt: new Date(),
        updatedAt: new Date(),
        ...args,
      },
    ],
    {
      timestamps: false,
    },
  );
  return tags[0];
}

import * as faker from 'faker';
import { Tag, TagModel } from './tag.model';

export async function TagSeed(args?: Partial<Tag>): Promise<Tag> {
  const tags = await TagModel.create(
    [
      {
        name: faker.lorem.word(),
        stat: {
          count: 0,
        },
        createdAt: new Date(),
        updatedAt: new Date(),
        ...args,
      },
    ],
    {
      timestamps: false,
    },
  );
  return tags[0];
}

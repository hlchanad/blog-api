import { UserSeed } from '@modules/user/models/user.seed';
import * as faker from 'faker';
import { Post, PostModel } from './post.model';
import { TagSeed } from '@modules/post/models/tag.seed';
import { CategorySeed } from '@modules/post/models/category.seed';

export async function PostSeed(args?: Partial<Post>): Promise<Post> {
  const posts = await PostModel.create(
    [
      {
        author: !args?.author && (await UserSeed()).id,
        category: !args?.category && (await CategorySeed()).id,
        title: faker.lorem.sentence(),
        content: faker.lorem.paragraph(),
        summary: faker.lorem.sentence(),
        banner: faker.image.imageUrl(),
        tags:
          !args?.tags &&
          (
            await Promise.all(
              Array.from(new Array(1 + faker.datatype.number(4))).map(() =>
                TagSeed({ name: faker.lorem.word() }),
              ),
            )
          ).map((tag) => tag.id),
        createdAt: new Date(),
        updatedAt: new Date(),
        ...args,
      },
    ],
    {
      timestamps: false,
    },
  );
  return posts[0];
}

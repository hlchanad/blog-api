import { getCollectionName } from '@helpers';
import * as mongoose from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import { nanoid } from 'nanoid';

export const CATEGORY_COLLECTION = getCollectionName('post', 'category');

export type Category = mongoose.Document & {
  _id: string;
  name: string;
  slug: string;
  createdAt: Date;
  updatedAt: Date;
};

export type ICategoryModel = mongoose.Model<Category>;

const schema = new mongoose.Schema<Category>(
  {
    _id: {
      type: String,
      required: false,
      default: () => nanoid(),
    },
    name: {
      type: String,
      required: true,
      index: true,
    },
    slug: {
      type: String,
      required: false,
      slug: 'name',
      unique: true,
    },
  },
  {
    timestamps: true,
  },
);

schema.plugin(slug);

export const CategoryModel = mongoose.model<Category, ICategoryModel>(
  'Category',
  schema,
  CATEGORY_COLLECTION,
);

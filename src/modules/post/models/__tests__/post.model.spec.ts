import { User, UserModel } from '@modules/user';
import { MongoInMemory } from '@test';
import * as mongoose from 'mongoose';
import { PostModel, SerializedPost } from '../post.model';
import { PostSeed } from '../post.seed';
import { Tag } from '../tag.model';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('tags', () => {
  it('populates tags', async () => {
    const post = await PostSeed();

    const result = await PostModel.findById(post.id).populate('tags');

    expect(result.tags).toHaveLength(post.tags.length);
    expect(result.tags.map((tag) => (tag as Tag).id)).toEqual(
      expect.arrayContaining(post.tags.map((tag: string) => tag)),
    );
  });
});

describe('author', () => {
  it('populates author', async () => {
    const post = await PostSeed();
    const author = await UserModel.findById(post.author);

    const result = await PostModel.findById(post.id).populate('author');

    expect((result.author as User).id).toBe(author.id);
  });
});

describe('serialize()', () => {
  it('returns SerializedPost', async () => {
    const post = await PostSeed();
    const result = post.serialize();

    const idOf = (obj) => (typeof obj === 'string' ? obj : obj.id);
    expect(result).toEqual(
      expect.objectContaining({
        id: post._id,
        author: idOf(post.author),
        category: idOf(post.category),
        title: post.title,
        content: post.content,
        summary: post.summary,
        banner: post.banner,
        slug: post.slug,
        tags: post.tags.map((tag) => idOf(tag)),
        createdAt: post.createdAt.toISOString(),
        updatedAt: post.updatedAt.toISOString(),
      } as SerializedPost),
    );
  });

  it('contains every keys of Post', async () => {
    const post = await PostSeed();
    const result = post.serialize();

    expect(Object.keys(result)).toEqual(
      expect.arrayContaining(
        Object.keys(PostModel.schema.obj).filter((key) => key !== '_id'),
      ),
    );
  });
});

import { getCollectionName } from '@helpers';
import { User } from '@modules/user';
import * as mongoose from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';
import * as slug from 'mongoose-slug-updater';
import { nanoid } from 'nanoid';
import { Category } from './category.model';
import { Tag } from './tag.model';

export const POST_COLLECTION = getCollectionName('post', 'post');

export interface SerializedPost {
  id: string;
  author: string;
  category: string;
  title: string;
  content: string;
  summary?: string;
  banner: string;
  slug: string;
  tags: string[];
  createdAt: string;
  updatedAt: string;
}

export type Post = mongoose.Document & {
  _id: string;
  author: string | User;
  category: string | Category;
  title: string;
  content: string;
  summary?: string;
  banner: string;
  slug: string;
  tags: (string | Tag)[];
  createdAt: Date;
  updatedAt: Date;

  serialize: () => SerializedPost;
};

export type IPostModel = mongoose.Model<Post> & {
  paginate: mongoose.PaginateModel<Post>['paginate'];
};

const schema = new mongoose.Schema<Post, IPostModel>(
  {
    _id: {
      type: String,
      required: false,
      default: () => nanoid(),
    },
    author: {
      type: String,
      required: true,
      index: true,
      ref: 'User',
    },
    category: {
      type: String,
      required: true,
      index: true,
      ref: 'Category',
    },
    title: { type: String, required: true },
    content: { type: String, required: true },
    summary: { type: String, required: false },
    banner: { type: String, required: true },
    slug: {
      type: String,
      required: false,
      slug: 'title',
      unique: true,
    },
    tags: {
      type: [String],
      required: false,
      default: () => [],
      ref: 'Tag',
    },
  },
  {
    timestamps: true,
  },
);

schema.plugin(slug);
schema.plugin(mongoosePaginate);

schema.methods.serialize = function (): SerializedPost {
  const idOf = (obj) => (typeof obj === 'string' ? obj : obj.id);
  return {
    id: this._id,
    author: idOf(this.author),
    category: idOf(this.category),
    title: this.title,
    content: this.content,
    summary: this.summary,
    banner: this.banner,
    slug: this.slug,
    tags: this.tags.map((tag) => idOf(tag)),
    createdAt: this.createdAt.toISOString(),
    updatedAt: this.updatedAt.toISOString(),
  };
};

export const PostModel = mongoose.model<Post, IPostModel>(
  'Post',
  schema,
  POST_COLLECTION,
);

import { getCollectionName } from '@helpers';
import * as mongoose from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import { nanoid } from 'nanoid';

export const TAG_COLLECTION = getCollectionName('post', 'tag');

export type Tag = mongoose.Document & {
  _id: string;
  name: string;
  slug: string;
  stat: {
    count: number;
  };
  createdAt: Date;
  updatedAt: Date;
};

export type ITagModel = mongoose.Model<Tag>;

const schema = new mongoose.Schema<Tag>(
  {
    _id: {
      type: String,
      required: false,
      default: () => nanoid(),
    },
    name: {
      type: String,
      required: true,
      index: true,
    },
    slug: {
      type: String,
      required: false,
      slug: 'name',
      unique: true,
    },
    stat: {
      type: {
        count: { type: Number, required: false, default: 0 },
      },
      default: {
        count: 0,
      },
    },
  },
  {
    timestamps: true,
  },
);

schema.plugin(slug);

export const TagModel = mongoose.model<Tag, ITagModel>(
  'Tag',
  schema,
  TAG_COLLECTION,
);

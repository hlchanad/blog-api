export enum CategoryScopes {
  CREATE = 'category:create',
}

export enum PostScopes {
  CREATE = 'post:create',
  UPDATE = 'post:update',
}

import { config } from '@helpers';
import { SerializedPost } from './models';

export const EVENT_POST_CREATED =
  config<string>('sns.topics.post-created.arn') ?? 'post:created';
export interface EventPostCreated {
  post: SerializedPost;
}

export const EVENT_POST_UPDATED =
  config<string>('sns.topics.post-updated.arn') ?? 'post:updated';
export interface EventPostUpdated {
  current: SerializedPost;
  last: SerializedPost;
}

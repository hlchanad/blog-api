import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { TagModel } from '../../models';
import { TagSeed } from '../../models/tag.seed';
import * as TagService from '../tag.service';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('findOrCreate()', () => {
  it('creates new tag', async () => {
    const tag = await TagService.findOrCreate({ name: faker.lorem.word() });
    const tags = await TagModel.find();

    expect(tags).toHaveLength(1);
    expect(tags[0].id).toBe(tag.id);
  });

  it('returns existing tag', async () => {
    const tag = await TagSeed();

    const result = await TagService.findOrCreate({ name: tag.name });
    const tags = await TagModel.find();

    expect(tags).toHaveLength(1);
    expect(result.id).toBe(tag.id);
  });
});

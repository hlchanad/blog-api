import { pubsub } from '@helpers';
import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { EVENT_POST_CREATED, EVENT_POST_UPDATED } from '../../events';
import { Post, PostModel, TagModel } from '../../models';
import { PostSeed } from '../../models/post.seed';
import { TagSeed } from '../../models/tag.seed';
import * as PostService from '../post.service';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('createPost()', () => {
  it('creates post', async () => {
    const args = {
      author: faker.random.alphaNumeric(8),
      category: faker.random.alphaNumeric(8),
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
      tags: [faker.lorem.word()],
    };

    const post = await PostService.createPost(args);
    const result = await PostModel.findById(post.id);

    expect(result.author).toBe(args.author);
    expect(result.category).toBe(args.category);
    expect(result.title).toBe(args.title);
    expect(result.content).toBe(args.content);
    expect(result.summary).toBe(args.summary);
    expect(result.banner).toBe(args.banner);
  });

  describe('tags', () => {
    it('creates tags first, if not exist', async () => {
      const post = await PostService.createPost({
        author: faker.random.alphaNumeric(8),
        category: faker.random.alphaNumeric(8),
        title: faker.lorem.sentence(),
        content: faker.lorem.paragraph(),
        summary: faker.lorem.sentence(),
        banner: faker.image.imageUrl(),
        tags: [faker.lorem.word()],
      });

      const tags = await TagModel.find();
      expect(tags).toHaveLength(1);

      expect(post.tags).toHaveLength(1);
      expect(post.tags[0]).toBe(tags[0].id);
    });

    it('uses existing tags, if exists', async () => {
      const tag = await TagSeed();
      const post = await PostService.createPost({
        author: faker.random.alphaNumeric(8),
        category: faker.random.alphaNumeric(8),
        title: faker.lorem.sentence(),
        content: faker.lorem.paragraph(),
        summary: faker.lorem.sentence(),
        banner: faker.image.imageUrl(),
        tags: [tag.name],
      });

      const tags = await TagModel.find();
      expect(tags).toHaveLength(1);
      expect(tags[0].id).toBe(tag.id);

      expect(post.tags).toHaveLength(1);
      expect(post.tags[0]).toBe(tags[0].id);
    });
  });

  it('publishes post-created event', async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const publish = jest.spyOn(pubsub, 'publish');

    const args = {
      author: faker.random.alphaNumeric(8),
      category: faker.random.alphaNumeric(8),
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
      tags: [faker.lorem.word()],
    };

    const post = await PostService.createPost(args);

    expect(publish).toBeCalled();
    expect(publish).toBeCalledWith(EVENT_POST_CREATED, {
      post: post.serialize(),
    });

    publish.mockRestore();
  });
});

describe('updatePost()', () => {
  let post: Post;

  beforeEach(async () => {
    post = await PostSeed({ tags: [] });
  });

  it('updates the post', async () => {
    const args = {
      category: faker.random.alphaNumeric(8),
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
    };
    await PostService.updatePost(post.id, args);

    const result = await PostModel.findById(post.id);

    expect(result.category).toBe(args.category);
    expect(result.title).toBe(args.title);
    expect(result.content).toBe(args.content);
    expect(result.summary).toBe(args.summary);
    expect(result.banner).toBe(args.banner);
    expect(result.updatedAt).not.toEqual(result.createdAt);
  });

  describe('tags', () => {
    it('creates tags if not exist', async () => {
      const tags = [faker.lorem.word(), faker.lorem.word()];
      await PostService.updatePost(post.id, { tags });

      const result = await PostModel.findById(post.id);
      const tagDocs = await TagModel.find();

      expect(result.tags).toEqual(
        expect.arrayContaining(tagDocs.map((tag) => tag.id)),
      );

      expect(tagDocs).toHaveLength(tags.length);
      expect(tagDocs.map((tag) => tag.name)).toEqual(
        expect.arrayContaining(tags),
      );
    });

    it('uses existing tags if exists', async () => {
      const tags = await Promise.all([TagSeed(), TagSeed()]);
      await PostService.updatePost(post.id, {
        tags: tags.map((tag) => tag.name),
      });

      const result = await PostModel.findById(post.id);
      const tagDocs = await TagModel.find();

      expect(result.tags).toEqual(
        expect.arrayContaining(tagDocs.map((tag) => tag.id)),
      );

      expect(tagDocs).toHaveLength(tags.length);
      expect(tagDocs.map((tag) => tag.id)).toEqual(
        expect.arrayContaining(tags.map((tag) => tag.id)),
      );
    });
  });

  it('publishes post-updated event', async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    const publish = jest.spyOn(pubsub, 'publish');

    const args = {
      category: faker.random.alphaNumeric(8),
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
    };
    const updated = await PostService.updatePost(post.id, args);

    expect(publish).toBeCalled();
    expect(publish).toBeCalledWith(EVENT_POST_UPDATED, {
      current: updated.serialize(),
      last: post.serialize(),
    });

    publish.mockRestore();
  });
});

describe('findNext()', () => {
  let posts: Post[];
  beforeEach(async () => {
    posts = await Promise.all([
      PostSeed({ createdAt: new Date('2021-01-01') }),
      PostSeed({ createdAt: new Date('2021-01-02') }),
      PostSeed({ createdAt: new Date('2021-01-03') }),
    ]);
  });

  it('returns next post', async () => {
    const next = await PostService.findNext(posts[0]);
    expect(next.id).toBe(posts[1].id);

    const nothing = await PostService.findNext(posts[2]);
    expect(nothing).toBeNull();
  });
});

describe('findPrevious()', () => {
  let posts: Post[];
  beforeEach(async () => {
    posts = await Promise.all([
      PostSeed({ createdAt: new Date('2021-01-01') }),
      PostSeed({ createdAt: new Date('2021-01-02') }),
      PostSeed({ createdAt: new Date('2021-01-03') }),
    ]);
  });

  it('returns previous post', async () => {
    const next = await PostService.findPrevious(posts[2]);
    expect(next.id).toBe(posts[1].id);

    const nothing = await PostService.findPrevious(posts[0]);
    expect(nothing).toBeNull();
  });
});

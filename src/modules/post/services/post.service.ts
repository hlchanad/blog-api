import { pubsub } from '@helpers';
import { Post, PostModel } from '../models';
import * as TagService from './tag.service';
import {
  EVENT_POST_CREATED,
  EVENT_POST_UPDATED,
  EventPostCreated,
  EventPostUpdated,
} from '../events';

export async function createPost(args: {
  author: string;
  category: string;
  title: string;
  content: string;
  summary?: string;
  banner: string;
  tags?: string[];
}): Promise<Post> {
  const tagDocs = await Promise.all(
    (args.tags ?? []).map(async (name) => TagService.findOrCreate({ name })),
  );
  const post = await PostModel.create({
    ...args,
    tags: tagDocs.map((tag) => tag.id),
  });

  await pubsub.publish(EVENT_POST_CREATED, {
    post: post.serialize(),
  } as EventPostCreated);

  return post;
}

export async function updatePost(
  id: string,
  args: {
    category?: string;
    title?: string;
    content?: string;
    summary?: string;
    banner?: string;
    tags?: string[];
  },
): Promise<Post> {
  const original = await PostModel.findById(id);

  const { category, title, content, summary, banner } = args;
  const tagDocs = await Promise.all(
    (args.tags ?? []).map(async (name) => TagService.findOrCreate({ name })),
  );
  const updated = await PostModel.findOneAndUpdate(
    { _id: id },
    {
      $set: {
        ...(category && { category }),
        ...(title && { title }),
        ...(content && { content }),
        ...(summary && { summary }),
        ...(banner && { banner }),
        tags: tagDocs?.length ? tagDocs : [],
      },
    },
    { new: true },
  );

  await pubsub.publish(EVENT_POST_UPDATED, {
    current: updated.serialize(),
    last: original.serialize(),
  } as EventPostUpdated);

  return updated;
}

export async function findNext(post: Post): Promise<Post> {
  return PostModel.findOne({
    createdAt: { $gt: post.createdAt },
  }).sort({ createdAt: 1 });
}

export async function findPrevious(post: Post): Promise<Post> {
  return PostModel.findOne({
    createdAt: { $lt: post.createdAt },
  }).sort({ createdAt: -1 });
}

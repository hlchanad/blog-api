import { Tag, TagModel } from '../models';

export async function findOrCreate({ name }: { name: string }): Promise<Tag> {
  const tag = await TagModel.findOne({ name });
  return tag ?? (await TagModel.create({ name }));
}

import { RecordNotFoundError } from '@modules/graphql';
import { schemaComposer } from 'graphql-compose';
import { CategoryModel } from '../../models';
import { PostService } from '../../services';
import { PostTC } from '../types';

interface IArgs {
  category: string;
  title: string;
  content: string;
  summary?: string;
  banner: string;
  tags?: string[];
}

export const createPost = schemaComposer.createResolver<any, IArgs>({
  type: PostTC,
  name: 'createPost',
  kind: 'mutation',
  description: 'Create post',
  args: {
    category: 'String!',
    title: 'String!',
    content: 'String!',
    summary: 'String',
    banner: 'String!',
    tags: '[String!]',
  },
  resolve: async ({
    args: { category, title, content, summary, banner, tags },
    context: { user },
  }) => {
    const isCategoryExists = await CategoryModel.exists({ _id: category });
    if (!isCategoryExists) {
      throw new RecordNotFoundError('category');
    }

    return PostService.createPost({
      author: user.id,
      category,
      title,
      content,
      summary,
      banner,
      tags,
    });
  },
});

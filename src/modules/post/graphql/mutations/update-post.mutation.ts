import { NotAuthorizedError, RecordNotFoundError } from '@modules/graphql';
import { GraphQLError } from 'graphql';
import { schemaComposer } from 'graphql-compose';
import { CategoryModel, PostModel } from '../../models';
import { PostService } from '../../services';
import { PostTC } from '../types';

interface IArgs {
  id: string;
  category?: string;
  title?: string;
  content?: string;
  summary?: string;
  banner?: string;
  tags?: string[];
}

export const updatePost = schemaComposer.createResolver<any, IArgs>({
  type: PostTC,
  name: 'updatePost',
  kind: 'mutation',
  description: 'Update post',
  args: {
    id: 'String!',
    category: 'String',
    title: 'String',
    content: 'String',
    summary: 'String',
    banner: 'String',
    tags: '[String!]',
  },
  resolve: async ({
    args: { id, category, title, content, summary, banner, tags },
    context: { user },
  }) => {
    const post = await PostModel.findById(id);
    if (!post) {
      throw new RecordNotFoundError('post');
    }

    if (post.author !== user.id) {
      throw new NotAuthorizedError();
    }

    if (category) {
      const isCategoryExists = await CategoryModel.exists({ _id: category });
      if (!isCategoryExists) {
        throw new GraphQLError('category not exist');
      }
    }

    return PostService.updatePost(id, {
      category,
      title,
      content,
      summary,
      banner,
      tags,
    });
  },
});

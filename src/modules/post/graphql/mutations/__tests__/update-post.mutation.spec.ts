import { MongoInMemory } from '@test';
import { UserModel } from '@modules/user';
import { UserSeed } from '@modules/user/models/user.seed';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { PostModel, TagModel } from '../../../models';
import { PostSeed } from '../../../models/post.seed';
import { updatePost } from '../update-post.mutation';

describe('resolver args', () => {
  it('should have optional arg `category`', async () => {
    expect(updatePost.getArgTypeName('category')).toBe('String');
  });

  it('should have optional arg `title`', async () => {
    expect(updatePost.getArgTypeName('title')).toBe('String');
  });

  it('should have optional arg `content`', async () => {
    expect(updatePost.getArgTypeName('content')).toBe('String');
  });

  it('should have optional arg `summary`', async () => {
    expect(updatePost.getArgTypeName('summary')).toBe('String');
  });

  it('should have optional arg `banner`', async () => {
    expect(updatePost.getArgTypeName('banner')).toBe('String');
  });

  it('should have optional arg `tags`', async () => {
    expect(updatePost.getArgTypeName('tags')).toBe('[String!]');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('throws error if post not found', async () => {
    const args = {
      id: faker.random.alphaNumeric(8),
    };
    const context = {
      user: await UserSeed(),
    };

    await expect(() => updatePost.resolve({ args, context })).rejects.toThrow(
      'post not found',
    );
  });

  it('throws error if author is not current user', async () => {
    const post = await PostSeed();
    const args = {
      id: post.id,
    };
    const context = {
      user: await UserSeed(),
    };

    await expect(() => updatePost.resolve({ args, context })).rejects.toThrow(
      'not authorized',
    );
  });

  it('throws error if category not found', async () => {
    const post = await PostSeed();
    const args = {
      id: post.id,
      category: faker.random.alphaNumeric(8),
    };
    const context = {
      user: await UserModel.findById(post.author as string),
    };

    await expect(() => updatePost.resolve({ args, context })).rejects.toThrow(
      'category not exist',
    );
  });

  it('updates a post record', async () => {
    const post = await PostSeed();
    const args = {
      id: post.id,
      category: post.category as string,
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
      tags: [faker.lorem.word()],
    };
    const context = {
      user: await UserModel.findById(post.author as string),
    };

    await updatePost.resolve({ args, context });

    const tags = await TagModel.find();
    expect(tags).toHaveLength(post.tags.length + args.tags.length);

    const posts = await PostModel.find();
    expect(posts).toHaveLength(1);

    expect(posts[0].category).toBe(args.category);
    expect(posts[0].title).toBe(args.title);
    expect(posts[0].content).toBe(args.content);
    expect(posts[0].summary).toBe(args.summary);
    expect(posts[0].banner).toBe(args.banner);
    expect(posts[0].tags).toEqual(
      expect.arrayContaining(
        tags
          .filter((tagDoc) => args.tags.find((tag) => tagDoc.name === tag))
          .map((tagDoc) => tagDoc.id),
      ),
    );
  });
});

import { MongoInMemory } from '@test';
import { UserSeed } from '@modules/user/models/user.seed';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { PostModel, TagModel } from '../../../models';
import { CategorySeed } from '../../../models/category.seed';
import { createPost } from '../create-post.mutation';

describe('resolver args', () => {
  it('should have required arg `category`', async () => {
    expect(createPost.getArgTypeName('category')).toBe('String!');
  });

  it('should have required arg `title`', async () => {
    expect(createPost.getArgTypeName('title')).toBe('String!');
  });

  it('should have required arg `content`', async () => {
    expect(createPost.getArgTypeName('content')).toBe('String!');
  });

  it('should have required arg `summary`', async () => {
    expect(createPost.getArgTypeName('summary')).toBe('String');
  });

  it('should have required arg `banner`', async () => {
    expect(createPost.getArgTypeName('banner')).toBe('String!');
  });

  it('should have required arg `tags`', async () => {
    expect(createPost.getArgTypeName('tags')).toBe('[String!]');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('throws error if category not found', async () => {
    const args = {
      category: faker.random.alphaNumeric(8),
    };
    const context = {
      user: await UserSeed(),
    };

    await expect(() => createPost.resolve({ args, context })).rejects.toThrow(
      'category not found',
    );
  });

  it('creates a post record', async () => {
    const category = await CategorySeed();
    const args = {
      category: category.id,
      title: faker.lorem.sentence(),
      content: faker.lorem.paragraph(),
      summary: faker.lorem.sentence(),
      banner: faker.image.imageUrl(),
      tags: [faker.lorem.word(), faker.lorem.word()],
    };
    const context = {
      user: await UserSeed(),
    };
    await createPost.resolve({ args, context });

    const tags = await TagModel.find();
    expect(tags).toHaveLength(2);
    expect(tags[0].name).toBe(args.tags[0]);
    expect(tags[1].name).toBe(args.tags[1]);

    const posts = await PostModel.find();
    expect(posts).toHaveLength(1);

    const post = posts[0];
    expect(post.author).toBe(context.user.id);
    expect(post.category).toBe(args.category);
    expect(post.title).toBe(args.title);
    expect(post.content).toBe(args.content);
    expect(post.summary).toBe(args.summary);
    expect(post.banner).toBe(args.banner);
    expect(post.tags).toMatchObject(tags.map((tag) => tag.id));
  });
});

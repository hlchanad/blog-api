import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { CategoryModel } from '../../../models';
import { createCategory } from '../create-category.mutation';

describe('resolver args', () => {
  it('should have required arg `name`', async () => {
    expect(createCategory.getArgTypeName('name')).toBe('String!');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('creates a category record', async () => {
    const args = {
      name: faker.lorem.word(),
    };
    await createCategory.resolve({ args });

    const categories = await CategoryModel.find();
    expect(categories).toHaveLength(1);
    expect(categories[0].name).toBe(args.name);
  });
});

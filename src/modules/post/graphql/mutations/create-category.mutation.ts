import { schemaComposer } from 'graphql-compose';
import { CategoryModel } from '../../models';
import { CategoryTC } from '../types';

interface IArgs {
  name: string;
}

export const createCategory = schemaComposer.createResolver<any, IArgs>({
  type: CategoryTC,
  name: 'createCategory',
  kind: 'mutation',
  description: 'Create category',
  args: {
    name: 'String!',
  },
  resolve: async ({ args: { name } }) => {
    return CategoryModel.create({ name });
  },
});

import {
  getPageableResult,
  getPageableTC,
  getValuesFromNumericEnum,
} from '@modules/graphql';
import { schemaComposer } from 'graphql-compose';
import { QuerySelector } from 'mongoose';
import { TagModel } from '../../models';
import { TagTC } from '../types';

enum TagsSort {
  POPULAR = 'POPULAR',
}

const TagsSortTC = schemaComposer.createEnumTC({
  name: 'TagsSortEnum',
  values: getValuesFromNumericEnum(TagsSort),
});

interface IArgs {
  filter?: {
    name?: string;
  };
  sort: TagsSort;
  offset: number;
  limit: number;
}

export const tags = schemaComposer.createResolver<any, IArgs>({
  type: getPageableTC(TagTC),
  name: 'tags',
  kind: 'query',
  description: 'List tags with pagination',
  args: {
    filter: schemaComposer.createInputTC({
      name: 'TagsFilterInput',
      fields: {
        name: 'String',
      },
    }),
    sort: { type: TagsSortTC, defaultValue: TagsSort.POPULAR },
    offset: { type: 'Int', defaultValue: 0 },
    limit: { type: 'Int', defaultValue: 10 },
  },
  resolve: async ({ args: { filter, sort, offset, limit } }) => {
    const common = commonAggregations(filter);

    const [docs, totalCount] = await Promise.all([
      TagModel.aggregate([
        ...common,
        { $addFields: { id: '$_id' } }, // mimic mongoose model
        {
          $sort: (() => {
            switch (sort) {
              default:
              case TagsSort.POPULAR:
                return { 'stat.count': -1, updatedAt: -1 };
            }
          })(),
        },
        { $skip: offset },
        { $limit: limit },
      ]).exec(),
      TagModel.aggregate([...common, { $count: 'count' }])
        .exec()
        .then((result) => result[0]?.count ?? 0),
    ]);

    return getPageableResult({
      docs,
      totalDocs: totalCount,
      hasPrevPage: offset > 0 && offset < totalCount,
      hasNextPage: offset + limit < totalCount,
      limit,
      totalPages: Math.ceil(totalCount / limit),
      pagingCounter: 0,
    });
  },
});

function regexMatch(
  pattern: string,
  options: 'i' | 'm' | 'x' | 's' = 'i',
): QuerySelector<any> {
  return { $regex: pattern, $options: options };
}

function commonAggregations(filter: IArgs['filter']) {
  const $or: any[] = [
    ...(filter?.name ? [{ name: regexMatch(filter.name) }] : []),
  ];

  return [
    {
      $match: {
        ...($or.length && { $or }),
      },
    },
  ];
}

import { GraphQLError } from 'graphql';
import { schemaComposer } from 'graphql-compose';
import * as xor from 'xor-js';
import { PostModel } from '../../models';
import { PostTC } from '../types';

interface IArgs {
  id?: string;
  slug?: string;
}

export const post = schemaComposer.createResolver<any, IArgs>({
  type: PostTC,
  name: 'post',
  kind: 'query',
  description: 'Get post by id or slug',
  args: {
    id: 'String',
    slug: 'String',
  },
  resolve: async ({ args: { id, slug } }) => {
    if (!xor(id, slug)) {
      throw new GraphQLError('Exactly one of id or slug should be provided');
    }

    return PostModel.findOne({
      ...(id && { _id: id }),
      ...(slug && { slug }),
    });
  },
});

import { MongoInMemory } from '@test';
import { EnumTypeComposer } from 'graphql-compose';
import 'jest-extended';
import * as mongoose from 'mongoose';
import { Tag } from '../../../models';
import { TagSeed } from '../../../models/tag.seed';
import { tags as tagsQuery } from '../tags.query';

describe('resolver args', () => {
  it('should have optional arg `filter`', async () => {
    // regex: not end with '!'
    expect(tagsQuery.getArgTypeName('filter')).toMatch(/.*(?<!!)$/);

    const filterTC = tagsQuery.getArgITC('filter');
    expect(filterTC.getFieldTypeName('name')).toBe('String');
  });

  it('should have optional arg `sort`', async () => {
    const sortEnum = (
      tagsQuery.getArgTC('sort') as EnumTypeComposer
    ).getFieldNames();
    const expectedEnum = ['POPULAR'];
    expect(sortEnum).toIncludeAllMembers(expectedEnum);
    expect(expectedEnum).toIncludeAllMembers(sortEnum);
  });

  it('should have optional arg `offer`', async () => {
    expect(tagsQuery.getArgTypeName('offset')).toBe('Int');
  });

  it('should have optional arg `limit`', async () => {
    expect(tagsQuery.getArgTypeName('limit')).toBe('Int');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;
  let tags: Tag[];

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());

    tags = await Promise.all([
      TagSeed({ stat: { count: 100 } }),
      TagSeed({ stat: { count: 200 } }),
    ]);
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  describe('filter', () => {
    it('returns all posts if no filter', async () => {
      const { edges } = await tagsQuery.resolve({
        args: { offset: 0, limit: 1000 },
      });
      expect(edges).toHaveLength(tags.length);
    });

    it('filters by name', async () => {
      const { edges } = await tagsQuery.resolve({
        args: {
          filter: { name: tags[0].name },
          offset: 0,
          limit: 1000,
        },
      });
      expect(edges).toHaveLength(1);
      expect(edges[0].id).toBe(tags[0].id);
    });
  });

  it('skips records', async () => {
    const { edges } = await tagsQuery.resolve({
      args: {
        offset: 1000,
        limit: 1000,
      },
    });
    expect(edges).toHaveLength(0);
  });

  it('limits records', async () => {
    const { edges } = await tagsQuery.resolve({
      args: {
        offset: 0,
        limit: 1,
      },
    });
    expect(edges).toHaveLength(1);
  });

  it('sorts records', async () => {
    const { edges } = await tagsQuery.resolve({
      args: {
        sort: 'POPULAR',
        offset: 0,
        limit: 1000,
      },
    });

    expect(edges).toHaveLength(tags.length);
    expect(edges[0].id).toBe(tags[1].id);
    expect(edges[1].id).toBe(tags[0].id);
  });
});

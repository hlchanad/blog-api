import { MongoInMemory } from '@test';
import * as mongoose from 'mongoose';
import { Post } from '../../../models';
import { PostSeed } from '../../../models/post.seed';
import { post as postQuery } from '../post.query';

describe('resolver args', () => {
  it('should have optional arg `id`', async () => {
    expect(postQuery.getArgTypeName('id')).toBe('String');
  });

  it('should have optional arg `slug`', async () => {
    expect(postQuery.getArgTypeName('slug')).toBe('String');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;
  let posts: Post[];

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();

    const dbUri = mongoServer.getConnectionString();

    await mongoose.connect(dbUri);

    posts = await Promise.all([PostSeed(), PostSeed()]);
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('rejects if no args provided', async () => {
    await expect(postQuery.resolve({ args: {} })).rejects.toThrow(
      'Exactly one of id or slug should be provided',
    );
  });

  it('gets post by `id`', async () => {
    const post = await postQuery.resolve({ args: { id: posts[0].id } });
    expect(post).not.toBeNull();
  });

  it('gets post by `slug`', async () => {
    const post = await postQuery.resolve({ args: { slug: posts[1].slug } });
    expect(post).not.toBeNull();
  });
});

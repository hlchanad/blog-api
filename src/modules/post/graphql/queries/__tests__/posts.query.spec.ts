import { UserModel } from '@modules/user';
import { MongoInMemory } from '@test';
import { EnumTypeComposer } from 'graphql-compose';
import 'jest-extended';
import * as mongoose from 'mongoose';
import { CategoryModel, Post, Tag } from '../../../models';
import { PostSeed } from '../../../models/post.seed';
import { TagSeed } from '../../../models/tag.seed';
import { posts as postsQuery } from '../posts.query';

describe('resolver args', () => {
  it('should have optional arg `filter`', async () => {
    // regex: not end with '!'
    expect(postsQuery.getArgTypeName('filter')).toMatch(/.*(?<!!)$/);

    const filterTC = postsQuery.schemaComposer.getITC('PostsFilterInput');
    expect(filterTC.getFieldTypeName('author')).toBe('String');
    expect(filterTC.getFieldTypeName('category')).toBe('String');
    expect(filterTC.getFieldTypeName('title')).toBe('String');
    expect(filterTC.getFieldTypeName('summary')).toBe('String');
    expect(filterTC.getFieldTypeName('tags')).toBe('[String!]');

    const queryTC = postsQuery.getArgITC('filter');
    expect(queryTC.getFieldTypeName('and')).toBe(filterTC.getTypeName());
    expect(queryTC.getFieldTypeName('and')).toBe(filterTC.getTypeName());
  });

  it('should have optional arg `sort`', async () => {
    const sortEnum = (
      postsQuery.getArgTC('sort') as EnumTypeComposer
    ).getFieldNames();
    const expectedEnum = ['LATEST_CREATED', 'LATEST_UPDATED'];
    expect(sortEnum).toIncludeAllMembers(expectedEnum);
    expect(expectedEnum).toIncludeAllMembers(sortEnum);
  });

  it('should have optional arg `offer`', async () => {
    expect(postsQuery.getArgTypeName('offset')).toBe('Int');
  });

  it('should have optional arg `limit`', async () => {
    expect(postsQuery.getArgTypeName('limit')).toBe('Int');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;
  let posts: Post[];
  let tags: Tag[];

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());

    tags = await Promise.all([
      TagSeed({ name: 'cheese' }),
      TagSeed({ name: 'chocolate' }),
      TagSeed({ name: 'japanese' }),
    ]);

    posts = await Promise.all([
      PostSeed({
        title: 'brownie',
        tags: [tags.find((tag) => tag.name === 'chocolate')],
        createdAt: new Date('2021-01-01'),
        updatedAt: new Date('2021-02-03'),
      }),
      PostSeed({
        title: 'new york cheese cake',
        tags: [tags.find((tag) => tag.name === 'cheese')],
        createdAt: new Date('2021-01-02'),
        updatedAt: new Date('2021-02-02'),
      }),
      PostSeed({
        title: 'japanese cheese cake',
        tags: [
          tags.find((tag) => tag.name === 'cheese'),
          tags.find((tag) => tag.name === 'japanese'),
        ],
        createdAt: new Date('2021-01-03'),
        updatedAt: new Date('2021-02-01'),
      }),
    ]);
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  describe('filter', () => {
    it('returns all posts if no filter', async () => {
      const { edges } = await postsQuery.resolve({
        args: { offset: 0, limit: 1000 },
      });
      expect(edges).toHaveLength(posts.length);
    });

    it('filters by category', async () => {
      const category = await CategoryModel.findById(posts[0].category);

      await Promise.all(
        ['and', 'or'].map(async (operator) => {
          const { edges } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { category: posts[0].category } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges).toHaveLength(1);
          expect(edges[0].id).toBe(posts[0].id);

          const { edges: edges2 } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { category: category.name } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges2).toHaveLength(1);
          expect(edges2[0].id).toBe(posts[0].id);
        }),
      );
    });

    it('filters by author', async () => {
      const author = await UserModel.findById(posts[0].author);

      await Promise.all(
        ['and', 'or'].map(async (operator) => {
          const { edges } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { author: posts[0].author } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges).toHaveLength(1);
          expect(edges[0].id).toBe(posts[0].id);

          const { edges: edges2 } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { author: author.username } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges2).toHaveLength(1);
          expect(edges2[0].id).toBe(posts[0].id);

          const { edges: edges3 } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { author: author.firstname } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges3).toHaveLength(1);
          expect(edges3[0].id).toBe(posts[0].id);

          const { edges: edges4 } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { author: author.lastname } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges4).toHaveLength(1);
          expect(edges4[0].id).toBe(posts[0].id);
        }),
      );
    });

    it('filters by title', async () => {
      await Promise.all(
        ['and', 'or'].map(async (operator) => {
          const { edges } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { title: posts[0].title } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges).toHaveLength(1);
          expect(edges[0].id).toBe(posts[0].id);
        }),
      );
    });

    it('filters by summary', async () => {
      await Promise.all(
        ['and', 'or'].map(async (operator) => {
          const { edges } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { summary: posts[0].summary } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges).toHaveLength(1);
          expect(edges[0].id).toBe(posts[0].id);
        }),
      );
    });

    it('filters by tags', async () => {
      await Promise.all(
        ['and', 'or'].map(async (operator) => {
          const { edges } = await postsQuery.resolve({
            args: {
              filter: { [operator]: { tags: [posts[0].tags[0]] } },
              offset: 0,
              limit: 1000,
            },
          });
          expect(edges).toHaveLength(1);
          expect(edges[0].id).toBe(posts[0].id);
        }),
      );
    });

    describe('and/or combinations', () => {
      it('filters title of tag "cheese"', async () => {
        const { edges } = await postsQuery.resolve({
          args: {
            filter: {
              and: { tags: ['cheese'] },
              or: { title: 'new york' },
            },
            offset: 0,
            limit: 1000,
          },
        });
        expect(edges).toHaveLength(1);
        expect(edges[0].id).toBe(
          posts.find((post) => post.title === 'new york cheese cake').id,
        );
      });
    });
  });

  it('skips records', async () => {
    const { edges } = await postsQuery.resolve({
      args: {
        offset: 1000,
        limit: 1000,
      },
    });
    expect(edges).toHaveLength(0);
  });

  it('limits records', async () => {
    const { edges } = await postsQuery.resolve({
      args: {
        offset: 0,
        limit: 1,
      },
    });
    expect(edges).toHaveLength(1);
  });

  it('sorts records', async () => {
    const { edges: edges1 } = await postsQuery.resolve({
      args: {
        sort: 'LATEST_CREATED',
        offset: 0,
        limit: 1000,
      },
    });

    const { edges: edges2 } = await postsQuery.resolve({
      args: {
        sort: 'LATEST_UPDATED',
        offset: 0,
        limit: 1000,
      },
    });

    expect(edges1).toHaveLength(posts.length);
    expect(edges2).toHaveLength(posts.length);
    expect(edges1[0].id).not.toBe(edges2[0].id);
  });
});

import {
  getPageableResult,
  getPageableTC,
  getValuesFromNumericEnum,
} from '@modules/graphql';
import { USER_COLLECTION } from '@modules/user';
import { schemaComposer } from 'graphql-compose';
import { QuerySelector } from 'mongoose';
import { CATEGORY_COLLECTION, PostModel, TAG_COLLECTION } from '../../models';
import { PostTC } from '../types';

enum PostsSort {
  LATEST_CREATED = 'LATEST_CREATED',
  LATEST_UPDATED = 'LATEST_UPDATED',
}

const PostsSortTC = schemaComposer.createEnumTC({
  name: 'PostsSortEnum',
  values: getValuesFromNumericEnum(PostsSort),
});

interface Filter {
  author?: string;
  category?: string;
  title?: string;
  summary?: string;
  tags?: string[];
}

interface IArgs {
  filter?: {
    and?: Filter;
    or?: Filter;
  };
  sort: PostsSort;
  offset: number;
  limit: number;
}

const PostsFilterInput = schemaComposer.createInputTC({
  name: 'PostsFilterInput',
  fields: {
    author: 'String',
    category: 'String',
    title: 'String',
    summary: 'String',
    tags: '[String!]',
  },
});

export const posts = schemaComposer.createResolver<any, IArgs>({
  type: getPageableTC(PostTC),
  name: 'posts',
  kind: 'query',
  description: 'List posts with pagination',
  args: {
    filter: schemaComposer.createInputTC({
      name: 'PostsFilterQueryInput',
      fields: {
        and: PostsFilterInput,
        or: PostsFilterInput,
      },
    }),
    sort: { type: PostsSortTC, defaultValue: PostsSort.LATEST_CREATED },
    offset: { type: 'Int', defaultValue: 0 },
    limit: { type: 'Int', defaultValue: 10 },
  },
  resolve: async ({ args: { filter, sort, offset, limit } }) => {
    const common = commonAggregations(filter);

    const [docs, totalCount] = await Promise.all([
      PostModel.aggregate([
        ...common,
        { $addFields: { id: '$_id' } }, // mimic mongoose model
        {
          $sort: (() => {
            switch (sort) {
              case PostsSort.LATEST_UPDATED:
                return { updatedAt: -1 };
              default:
              case PostsSort.LATEST_CREATED:
                return { createdAt: -1 };
            }
          })(),
        },
        { $skip: offset },
        { $limit: limit },
      ]).exec(),
      PostModel.aggregate([...common, { $count: 'count' }])
        .exec()
        .then((result) => result[0]?.count ?? 0),
    ]);

    return getPageableResult({
      docs,
      totalDocs: totalCount,
      hasPrevPage: offset > 0 && offset < totalCount,
      hasNextPage: offset + limit < totalCount,
      limit,
      totalPages: Math.ceil(totalCount / limit),
      pagingCounter: 0,
    });
  },
});

function regexMatch(
  pattern: string,
  options: 'i' | 'm' | 'x' | 's' = 'i',
): QuerySelector<any> {
  return { $regex: pattern, $options: options };
}

function commonAggregations(filter: IArgs['filter']) {
  const $or: any[] = [
    ...(filter?.or?.author
      ? [
          { 'author._id': filter.or.author },
          { 'author.firstname': regexMatch(filter.or.author) },
          { 'author.lastname': regexMatch(filter.or.author) },
          { 'author.username': regexMatch(filter.or.author) },
        ]
      : []),
    ...(filter?.or?.category
      ? [
          { 'category._id': filter.or.category },
          { 'category.name': regexMatch(filter.or.category) },
        ]
      : []),
    ...(filter?.or?.tags
      ? filter.or.tags
          .map((tag) => [{ 'tags._id': tag }, { 'tags.name': regexMatch(tag) }])
          .flat()
      : []),
    ...(filter?.or?.title ? [{ title: regexMatch(filter.or.title) }] : []),
    ...(filter?.or?.summary
      ? [{ summary: regexMatch(filter.or.summary) }]
      : []),
  ];

  const $and: any[] = [
    ...($or.length ? [{ $or }] : []),
    ...(filter?.and?.author
      ? [
          {
            $or: [
              { 'author._id': filter.and.author },
              { 'author.firstname': regexMatch(filter.and.author) },
              { 'author.lastname': regexMatch(filter.and.author) },
              { 'author.username': regexMatch(filter.and.author) },
            ],
          },
        ]
      : []),
    ...(filter?.and?.category
      ? [
          {
            $or: [
              { 'category._id': filter.and.category },
              { 'category.name': regexMatch(filter.and.category) },
            ],
          },
        ]
      : []),
    ...(filter?.and?.tags
      ? [
          {
            $or: filter.and.tags
              .map((tag) => [
                { 'tags._id': tag },
                { 'tags.name': regexMatch(tag) },
              ])
              .flat(),
          },
        ]
      : []),
    ...(filter?.and?.title ? [{ title: regexMatch(filter.and.title) }] : []),
    ...(filter?.and?.summary
      ? [{ summary: regexMatch(filter.and.summary) }]
      : []),
  ];

  return [
    ...(filter?.or?.author || filter?.and?.author
      ? [
          {
            $lookup: {
              from: USER_COLLECTION,
              localField: 'author',
              foreignField: '_id',
              as: 'author',
            },
          },
          { $unwind: '$author' },
        ]
      : []),
    ...(filter?.or?.category || filter?.and?.category
      ? [
          {
            $lookup: {
              from: CATEGORY_COLLECTION,
              localField: 'category',
              foreignField: '_id',
              as: 'category',
            },
          },
          { $unwind: '$category' },
        ]
      : []),
    ...(filter?.or?.tags || filter?.and?.tags
      ? [
          {
            $lookup: {
              from: TAG_COLLECTION,
              localField: 'tags',
              foreignField: '_id',
              as: 'tags',
            },
          },
        ]
      : []),
    {
      $match: {
        ...($and.length && { $and }),
      },
    },
  ];
}

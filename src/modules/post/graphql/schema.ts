import { requireLogin } from '@modules/auth';
import { CategoryScopes, PostScopes } from '../scopes';
import { createCategory, createPost, updatePost } from './mutations';
import { post, posts, tags } from './queries';

export const PostQuery = {
  post,
  posts,
  tags,
};

export const PostMutation = {
  createCategory: createCategory.wrap(requireLogin([CategoryScopes.CREATE])),
  createPost: createPost.wrap(requireLogin([PostScopes.CREATE])),
  updatePost: updatePost.wrap(requireLogin([PostScopes.UPDATE])),
};

import { summaryOf } from '@helpers';
import { getIdField, TimestampsTC } from '@modules/graphql';
import { User, UserModel, UserTC } from '@modules/user';
import { schemaComposer } from 'graphql-compose';
import { MongooseDataloaderFactory } from 'graphql-dataloader-mongoose';
import { Category, CategoryModel, Post, Tag, TagModel } from '../../models';
import { PostService } from '../../services';
import { CategoryTC } from './category.tc';
import { TagTC } from './tag.tc';
import { GraphQLError } from 'graphql';

export const PostTC = schemaComposer.createObjectTC({
  name: 'Post',
  interfaces: [TimestampsTC],
  fields: {
    id: getIdField(),
    author: {
      type: UserTC.NonNull,
      resolve: async (
        post: Post,
        args,
        { dataloader }: { dataloader: MongooseDataloaderFactory },
      ) =>
        typeof post.author === 'string'
          ? await dataloader
              .mongooseLoader<User>(UserModel)
              .dataloader('_id')
              .load(post.author as string)
          : post.author,
    },
    category: {
      type: CategoryTC.NonNull,
      resolve: async (
        post: Post,
        args,
        { dataloader }: { dataloader: MongooseDataloaderFactory },
      ) =>
        typeof post.category === 'string'
          ? await dataloader
              .mongooseLoader<Category>(CategoryModel)
              .dataloader('_id')
              .load(post.category as string)
          : post.category,
    },
    title: 'String!',
    content: 'String!',
    summary: {
      type: 'String!',
      resolve: (post: Post) => post.summary ?? summaryOf(post.content, 200),
    },
    banner: 'String!',
    slug: 'String!',
    tags: {
      type: TagTC.NonNull.List,
      resolve: async (
        post: Post,
        args,
        { dataloader }: { dataloader: MongooseDataloaderFactory },
      ) =>
        post.tags.length
          ? typeof post.tags[0] === 'string'
            ? await dataloader
                .mongooseLoader<Tag>(TagModel)
                .dataloader('_id')
                .loadMany(post.tags as string[])
            : post.tags
          : [],
    },
    createdAt: 'Date!',
    updatedAt: 'Date!',
  },
});

PostTC.addFields({
  previous: {
    type: PostTC,
    resolve: async (post: Post, args, context, info) => {
      if (info.path?.prev?.key !== 'post') {
        throw new GraphQLError('previous is only for post query');
      }
      return PostService.findPrevious(post);
    },
  },
  next: {
    type: PostTC,
    resolve: async (post: Post, args, context, info) => {
      if (info.path?.prev?.key !== 'post') {
        throw new GraphQLError('next is only for post query');
      }
      return PostService.findNext(post);
    },
  },
});

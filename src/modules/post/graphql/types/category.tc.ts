import { getIdField, TimestampsTC } from '@modules/graphql';
import { schemaComposer } from 'graphql-compose';

export const CategoryTC = schemaComposer.createObjectTC({
  name: 'Category',
  interfaces: [TimestampsTC],
  fields: {
    id: getIdField(),
    name: 'String!',
    slug: 'String!',
    createdAt: 'Date!',
    updatedAt: 'Date!',
  },
});

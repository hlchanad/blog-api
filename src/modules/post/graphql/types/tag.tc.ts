import { getIdField, TimestampsTC } from '@modules/graphql';
import { schemaComposer } from 'graphql-compose';

export const TagTC = schemaComposer.createObjectTC({
  name: 'Tag',
  interfaces: [TimestampsTC],
  fields: {
    id: getIdField(),
    name: 'String!',
    slug: 'String!',
    stat: schemaComposer.createObjectTC({
      name: 'TagStat',
      fields: {
        count: 'Int!',
      },
    }).NonNull,
    createdAt: 'Date!',
    updatedAt: 'Date!',
  },
});

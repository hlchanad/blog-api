import { database } from '@database';
import { pubsub } from '@helpers';
import { EventPostUpdated } from '../events';
import { TagModel } from '../models';

export const handler = async (rawEvent, context?) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await database.mongo.connect();

  const event = pubsub.parseEvent<EventPostUpdated>(rawEvent);

  const sameTagIds = event.current.tags.filter(
    (tag) => event.last.tags.indexOf(tag) !== -1,
  );
  const deletedTagIds = event.last.tags.filter(
    (tag) => sameTagIds.indexOf(tag) === -1,
  );
  const newTagIds = event.current.tags.filter(
    (tag) => sameTagIds.indexOf(tag) === -1,
  );

  await TagModel.updateMany(
    { _id: { $in: newTagIds } },
    { $inc: { 'stat.count': 1 } },
  );
  await TagModel.updateMany(
    { _id: { $in: deletedTagIds } },
    { $inc: { 'stat.count': -1 } },
  );
};

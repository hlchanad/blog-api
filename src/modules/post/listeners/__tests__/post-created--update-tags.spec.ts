import { database } from '@database';
import { mockEvent, MongoInMemory } from '@test';
import * as mongoose from 'mongoose';
import { EventPostCreated } from '../../events';
import { TagModel } from '../../models';
import { PostSeed } from '../../models/post.seed';
import { handler } from '../post-created--update-tags';

let mongoServer: MongoInMemory.Server;
let spy: jest.SpyInstance;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
  spy = jest
    .spyOn(database.mongo, 'connect')
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    .mockImplementation(() => Promise.resolve());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
  spy.mockRestore();
});

it('increase tag.stat.count by 1 for each tags', async () => {
  const post = await PostSeed();
  const event = mockEvent({ post: post.serialize() } as EventPostCreated);

  await handler(event, {});

  const tags = await TagModel.find({ _id: { $in: post.tags } });
  tags.forEach((tag) => expect(tag.stat.count).toBe(1));
});

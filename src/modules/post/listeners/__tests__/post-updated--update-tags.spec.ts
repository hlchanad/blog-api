import { database } from '@database';
import { mockEvent, MongoInMemory } from '@test';
import * as mongoose from 'mongoose';
import { EventPostUpdated } from '../../events';
import { Post, PostModel, Tag, TagModel } from '../../models';
import { PostSeed } from '../../models/post.seed';
import { TagSeed } from '../../models/tag.seed';
import { handler } from '../post-updated--update-tags';

let mongoServer: MongoInMemory.Server;
let spy: jest.SpyInstance;

let post: Post;
let updatedPost: Post;
let tags: Tag[];

let deletedTagIds: string[];
let newTagIds: string[];
let sameTagIds: string[];

let event;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
  spy = jest
    .spyOn(database.mongo, 'connect')
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    .mockImplementation(() => Promise.resolve());

  tags = await Promise.all([
    TagSeed(),
    TagSeed(),
    TagSeed(),
    TagSeed(),
    TagSeed(),
  ]);

  deletedTagIds = [tags[0].id, tags[1].id];
  newTagIds = [tags[3].id, tags[4].id];
  sameTagIds = [tags[2].id];

  post = await PostSeed({ tags: [...deletedTagIds, ...sameTagIds] });
  await TagModel.updateMany(
    { _id: { $in: post.tags } },
    { $inc: { 'stat.count': 1 } },
  );

  updatedPost = await PostModel.findOneAndUpdate(
    { _id: post.id },
    {
      $set: { tags: [...sameTagIds, ...newTagIds] },
    },
    { new: true },
  );

  event = mockEvent({
    current: updatedPost.serialize(),
    last: post.serialize(),
  } as EventPostUpdated);
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
  spy.mockRestore();
});

it('increases tag.stat.count by 1 for new tags', async () => {
  await handler(event, {});

  const tags = await TagModel.find({ _id: { $in: newTagIds } });
  tags.forEach((tag) => expect(tag.stat.count).toBe(1));
});

it('subtracts tag.stat.count by 1 for deleted tags', async () => {
  await handler(event, {});

  const tags = await TagModel.find({ _id: { $in: deletedTagIds } });
  tags.forEach((tag) => expect(tag.stat.count).toBe(0));
});

it('does nothing for same tags', async () => {
  await handler(event, {});

  const tags = await TagModel.find({ _id: { $in: sameTagIds } });
  tags.forEach((tag) => expect(tag.stat.count).toBe(1));
});

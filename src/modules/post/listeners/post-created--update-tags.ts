import { database } from '@database';
import { pubsub } from '@helpers';
import { EventPostCreated } from '../events';
import { TagModel } from '../models';

export const handler = async (rawEvent, context?) => {
  context.callbackWaitsForEmptyEventLoop = false;
  await database.mongo.connect();

  const event = pubsub.parseEvent<EventPostCreated>(rawEvent);

  await TagModel.updateMany(
    { _id: { $in: event.post.tags } },
    { $inc: { 'stat.count': 1 } },
  );
};

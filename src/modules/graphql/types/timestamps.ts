import { schemaComposer } from 'graphql-compose';

export const TimestampsTC = schemaComposer.createInterfaceTC({
  name: 'Timestamps',
  fields: {
    createdAt: 'Date!',
    updatedAt: 'Date!',
  },
});

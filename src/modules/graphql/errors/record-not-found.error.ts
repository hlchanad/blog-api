import { GraphQLError } from 'graphql';

export class RecordNotFoundError extends GraphQLError {
  constructor(record = 'record') {
    super(`${record} not found`);
  }
}

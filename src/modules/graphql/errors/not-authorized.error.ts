import { GraphQLError } from 'graphql';

export class NotAuthorizedError extends GraphQLError {
  constructor() {
    super('not authorized');
  }
}

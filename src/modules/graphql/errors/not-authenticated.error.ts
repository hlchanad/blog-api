import { GraphQLError } from 'graphql';

export class NotAuthenticatedError extends GraphQLError {
  constructor() {
    super('not authenticated');
  }
}

import { ObjectTypeComposer, schemaComposer } from 'graphql-compose';
import { Document, PaginateResult } from 'mongoose';

export function getPageableTC(
  edgesTC: ObjectTypeComposer<Document>,
): ObjectTypeComposer {
  let PageInfoTC;
  try {
    PageInfoTC = schemaComposer.getOTC('PageInfo');
  } catch (error) {
    PageInfoTC = null;
  }

  return schemaComposer.createObjectTC({
    name: `${edgesTC.getTypeName()}Pageable`,
    fields: {
      edges: edgesTC.NonNull.List.NonNull,
      pageInfo:
        PageInfoTC ??
        schemaComposer.createObjectTC({
          name: 'PageInfo',
          fields: {
            hasNextPage: 'Boolean!',
            hasPreviousPage: 'Boolean!',
          },
        }).NonNull,
      totalCount: 'Int!',
    },
  });
}

export function getPageableResult<T extends Document>(
  paginateResult: PaginateResult<T>,
) {
  return {
    edges: paginateResult.docs,
    pageInfo: {
      hasPreviousPage: paginateResult.hasPrevPage,
      hasNextPage: paginateResult.hasNextPage,
    },
    totalCount: paginateResult.totalDocs,
  };
}

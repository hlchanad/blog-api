import {
  EnumTypeComposerValueConfigMapDefinition,
  ObjectTypeComposerFieldConfigDefinition,
} from 'graphql-compose';
import { Document } from 'mongoose';

export function getIdField<
  TSource extends Document,
  TContext,
  TArgs = any,
>(): ObjectTypeComposerFieldConfigDefinition<TSource, TContext, TArgs> {
  return {
    type: 'String!',
    resolve: (doc) => doc._id,
  };
}

export function getValuesFromNumericEnum(
  _enum,
): EnumTypeComposerValueConfigMapDefinition {
  return Object.keys(_enum)
    .filter((key) => isNaN(parseInt(key)))
    .reduce((res, key) => ({ ...res, [key]: { value: _enum[key] } }), {});
}

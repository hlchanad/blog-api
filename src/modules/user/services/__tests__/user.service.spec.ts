import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { UserSeed } from '../../models/user.seed';
import * as UserService from '../user.service';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('isUsernameUsed()', () => {
  it('returns false if not used', async () => {
    const isUsed = await UserService.isUsernameUsed('some-username');
    expect(isUsed).toBeFalsy();
  });

  it('returns true if used', async () => {
    const user = await UserSeed({ username: faker.random.alphaNumeric(8) });
    const isUsed = await UserService.isUsernameUsed(user.username);
    expect(isUsed).toBeTruthy();
  });
});

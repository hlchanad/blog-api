import { MongoInMemory } from '@test';
import * as jwt from 'jsonwebtoken';
import * as mongoose from 'mongoose';
import * as LoginService from '../login.service';
import { UserSeed } from '../../models/user.seed';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('getToken()', () => {
  it('returns token', async () => {
    const spy = jest.spyOn(jwt, 'sign');

    const user = await UserSeed();
    const token = LoginService.getToken(user);

    expect(token).toBeString();
    expect(spy).toBeCalled();
    expect(spy).toBeCalledWith(
      expect.objectContaining({ userId: user.id }),
      expect.toBeString(),
      expect.toBeObject(),
    );

    spy.mockRestore();
  });
});

describe('parseToken()', () => {
  it('returns user doc', async () => {
    const user = await UserSeed();
    const token = LoginService.getToken(user);

    const result = await LoginService.parseToken(token);

    expect(result.id).toBe(user.id);
    expect(result.username).toBe(user.username);
  });

  it('returns null if invalid token', async () => {
    const result = await LoginService.parseToken('some token');
    expect(result).toBeNull();
  });
});

import { UserModel } from '../models';

export async function isUsernameUsed(username: string): Promise<boolean> {
  const count = await UserModel.countDocuments({ username });
  return count > 0;
}

import { config } from '@helpers';
import * as jwt from 'jsonwebtoken';
import { User, UserModel } from '../models';

export function getToken(user: User): string {
  return jwt.sign(
    {
      userId: user.id,
    },
    config('app.secret'),
    {
      expiresIn: config('token.login.duration'),
      issuer: `${config('app.name')}:login-service`,
    },
  );
}

export async function parseToken(token: string): Promise<User> {
  let payload;
  try {
    payload = jwt.verify(token, config('app.secret'), {
      issuer: `${config('app.name')}:login-service`,
    });

    if (typeof payload === 'string') {
      return null;
    }
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      return null;
    }

    throw error;
  }

  return UserModel.findById(payload.userId);
}

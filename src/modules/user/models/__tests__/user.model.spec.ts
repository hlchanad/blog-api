import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { User, UserModel } from '../user.model';
import { UserSeed } from '../user.seed';

let mongoServer: MongoInMemory.Server;

beforeEach(async () => {
  mongoServer = await MongoInMemory.create();
  await mongoose.connect(mongoServer.getConnectionString());
  await UserModel.ensureIndexes();
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
});

describe('password', () => {
  it('hashes password before save', async () => {
    const password = faker.random.alphaNumeric(8);
    const user = await UserSeed({ password });
    expect(user.password).not.toBe(password);
  });

  it('verifies password', async () => {
    const password = faker.random.alphaNumeric(8);
    const user = await UserSeed({ password });
    expect(user.verifyPassword(password)).toBeTruthy();
    expect(user.verifyPassword('dummy')).toBeFalsy();
  });
});

describe('hasScopes()', () => {
  let user: User;

  beforeEach(async () => {
    user = await UserSeed({
      scopes: [
        { scope: 'forever' },
        { scope: 'future', expiredAt: faker.date.future() },
        { scope: 'past', expiredAt: faker.date.past() },
      ],
    });
  });

  it('accepts scope without expiredAt', () => {
    expect(user.hasScopes(['forever'])).toBeTruthy();
  });

  it('accepts scope with future expiredAt', () => {
    expect(user.hasScopes(['future'])).toBeTruthy();
  });

  it('rejects scope with past expiredAt', () => {
    expect(user.hasScopes(['past'])).toBeFalsy();
  });

  it('accepts multiple scopes', () => {
    expect(user.hasScopes(['forever', 'future'])).toBeTruthy();
  });
});

import * as faker from 'faker';
import { User, UserModel } from './user.model';

export async function UserSeed(args?: Partial<User>): Promise<User> {
  const users = await UserModel.create(
    [
      {
        username: faker.random.alphaNumeric(8),
        password: faker.random.alphaNumeric(8),
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        avatar: faker.image.imageUrl(),
        email: faker.internet.email(),
        contacts: [
          { type: 'github', url: `https://github.com/${faker.random.word()}` },
          {
            type: 'twitter',
            url: `https://twitter.com/${faker.random.word()}`,
          },
        ],
        description: faker.lorem.paragraphs(),
        isSuperUser: false,
        scopes: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        ...args,
      },
    ],
    {
      timestamps: false,
    },
  );
  return users[0];
}

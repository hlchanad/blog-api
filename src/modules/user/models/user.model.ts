import { getCollectionName } from '@helpers';
import * as bcrypt from 'bcrypt';
import * as mongoose from 'mongoose';
import * as moment from 'moment';
import { nanoid } from 'nanoid';

export const USER_COLLECTION = getCollectionName('user', 'user');

export type User = mongoose.Document & {
  _id: string;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  avatar?: string;
  email: string;
  contacts: Array<{
    type: string;
    url: string;
  }>;
  description?: string;
  isSuperUser: boolean;
  scopes: {
    scope: string;
    expiredAt?: Date;
  }[];
  createdAt: Date;
  updatedAt: Date;

  verifyPassword: (password: string) => boolean;
  hasScopes: (scopes: string[]) => boolean;
};

export type IUserModel = mongoose.Model<User> /* & {}*/;

const schema = new mongoose.Schema<User, IUserModel>(
  {
    _id: {
      type: String,
      required: false,
      default: () => nanoid(),
    },
    username: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    password: { type: String, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    avatar: { type: String, required: false },
    email: { type: String, required: true },
    contacts: {
      type: [
        {
          type: { type: String, required: true },
          url: { type: String, required: true },
        },
      ],
      required: true,
      default: () => [],
    },
    description: { type: String, required: false },
    isSuperUser: {
      type: Boolean,
      required: false,
      default: false,
    },
    scopes: {
      type: [
        {
          scope: { type: String, required: true },
          expiredAt: { type: Date, required: false },
        },
      ],
      default: [],
    },
  },
  {
    timestamps: true,
  },
);

schema.pre('save', async function () {
  if (!this.isModified('password')) return;
  this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(12));
});

schema.methods.verifyPassword = function (password: string): boolean {
  return bcrypt.compareSync(password, this.password);
};

schema.methods.hasScopes = function (scopes: string[]): boolean {
  return scopes.every((requiredScope) =>
    this.scopes.find(
      ({ scope: userScope, expiredAt }) =>
        userScope === requiredScope &&
        (!expiredAt || moment(expiredAt).isAfter(moment())),
    ),
  );
};

export const UserModel = mongoose.model<User, IUserModel>(
  'User',
  schema,
  USER_COLLECTION,
);

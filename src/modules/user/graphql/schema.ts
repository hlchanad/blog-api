import { createUser, login } from './mutations';
import { user } from './queries';

export const UserQuery = {
  user,
};

export const UserMutation = {
  createUser,
  login,
};

import { RecordNotFoundError } from '@modules/graphql';
import { schemaComposer } from 'graphql-compose';
import { User, UserModel } from '../../models';
import { LoginService } from '../../services';

interface IArgs {
  username: string;
  password: string;
}

export const login = schemaComposer.createResolver<any, IArgs>({
  type: schemaComposer.createObjectTC({
    name: 'Login',
    fields: {
      token: 'String!',
    },
  }),
  name: 'login',
  kind: 'mutation',
  description: 'User login',
  args: {
    username: 'String!',
    password: 'String!',
  },
  resolve: async ({ args: { username, password } }) => {
    const user: User = await UserModel.findOne({ username });

    if (!user || !user.verifyPassword(password)) {
      throw new RecordNotFoundError('user');
    }

    return { token: LoginService.getToken(user) };
  },
});

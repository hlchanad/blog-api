import { GraphQLError } from 'graphql';
import { schemaComposer } from 'graphql-compose';
import { User, UserModel } from '../../models';
import { UserService } from '../../services';
import { ContactInputTC, UserTC } from '../types';

interface IArgs {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  avatar?: string;
  email: string;
  contacts?: User['contacts'];
  description?: string;
}

export const createUser = schemaComposer.createResolver<any, IArgs>({
  type: UserTC,
  name: 'createUser',
  kind: 'mutation',
  description: 'Create user',
  args: {
    username: 'String!',
    password: 'String!',
    firstname: 'String!',
    lastname: 'String!',
    avatar: 'String',
    email: 'String!',
    contacts: ContactInputTC.NonNull.List,
    description: 'String',
  },
  resolve: async ({
    args: {
      username,
      password,
      firstname,
      lastname,
      avatar,
      email,
      contacts,
      description,
    },
  }) => {
    const isUsernameUsed = await UserService.isUsernameUsed(username);
    if (isUsernameUsed) {
      throw new GraphQLError('username used');
    }

    try {
      return await UserModel.create({
        username,
        password,
        firstname,
        lastname,
        avatar,
        email,
        contacts,
        description,
        // default scopes for general user
        scopes: [], // TODO: think how to pre-set this field
      });
    } catch (error) {
      if (error.keyPattern?.username) {
        throw new GraphQLError('username used');
      }
      throw error;
    }
  },
});

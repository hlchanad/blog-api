import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { User, UserModel } from '../../../models';
import { UserSeed } from '../../../models/user.seed';
import { UserService } from '../../../services';
import { createUser } from '../create-user.mutation';
import { ContactInputTC } from '../../types';

describe('resolver args', () => {
  it('should have required arg `username`', async () => {
    expect(createUser.getArgTypeName('username')).toBe('String!');
  });

  it('should have required arg `password`', async () => {
    expect(createUser.getArgTypeName('password')).toBe('String!');
  });

  it('should have required arg `firstname`', async () => {
    expect(createUser.getArgTypeName('firstname')).toBe('String!');
  });

  it('should have required arg `lastname`', async () => {
    expect(createUser.getArgTypeName('lastname')).toBe('String!');
  });

  it('should have required arg `avatar`', async () => {
    expect(createUser.getArgTypeName('avatar')).toBe('String');
  });

  it('should have required arg `email`', async () => {
    expect(createUser.getArgTypeName('email')).toBe('String!');
  });

  it('should have optional arg `contacts`', async () => {
    expect(createUser.getArgTypeName('contacts')).toBe(
      ContactInputTC.NonNull.List.getTypeName(),
    );
  });

  it('should have optional arg `description`', async () => {
    expect(createUser.getArgTypeName('description')).toBe('String');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());
    await UserModel.ensureIndexes();
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('throws error before creation if username is used', async () => {
    const user = await UserSeed({ username: faker.random.alphaNumeric(8) });

    const args = {
      username: user.username,
      password: faker.random.alphaNumeric(8),
      firstname: faker.name.firstName(),
      lastname: faker.name.firstName(),
      email: faker.internet.email(),
    };
    await expect(createUser.resolve({ args })).rejects.toThrow('username used');
  });

  it('throws error during creation if username is used', async () => {
    const spy = jest
      .spyOn(UserService, 'isUsernameUsed')
      .mockReturnValue(Promise.resolve(false));

    const user = await UserSeed({ username: faker.random.alphaNumeric(8) });

    const args = {
      username: user.username,
      password: faker.random.alphaNumeric(8),
      firstname: faker.name.firstName(),
      lastname: faker.name.firstName(),
      email: faker.internet.email(),
    };
    await expect(createUser.resolve({ args })).rejects.toThrow('username used');

    spy.mockRestore();
  });

  it('creates a user record', async () => {
    const args = {
      username: faker.random.alphaNumeric(8),
      password: faker.random.alphaNumeric(8),
      firstname: faker.name.firstName(),
      lastname: faker.name.firstName(),
      avatar: faker.image.imageUrl(),
      email: faker.internet.email(),
      contacts: [
        { type: 'github', url: `https://github.com/${faker.random.word()}` },
        { type: 'twitter', url: `https://twitter.com/${faker.random.word()}` },
      ],
      description: faker.lorem.paragraphs(),
    };
    await createUser.resolve({ args });

    const users: User[] = await UserModel.find();
    expect(users).toHaveLength(1);

    const user = users[0];
    expect(user.username).toBe(args.username);
    expect(user.firstname).toBe(args.firstname);
    expect(user.lastname).toBe(args.lastname);
    expect(user.avatar).toBe(args.avatar);
    expect(user.email).toBe(args.email);
    expect(user.contacts).toEqual(
      expect.arrayContaining(
        args.contacts.map((contact) => expect.objectContaining(contact)),
      ),
    );
    expect(user.description).toBe(args.description);
  });
});

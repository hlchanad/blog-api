import { MongoInMemory } from '@test';
import * as faker from 'faker';
import * as mongoose from 'mongoose';
import { UserModel } from '../../../models';
import { UserSeed } from '../../../models/user.seed';
import { LoginService } from '../../../services';
import { login } from '../login.mutation';

describe('resolver args', () => {
  it('should have required arg `username`', async () => {
    expect(login.getArgTypeName('username')).toBe('String!');
  });

  it('should have required arg `password`', async () => {
    expect(login.getArgTypeName('password')).toBe('String!');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();
    await mongoose.connect(mongoServer.getConnectionString());
    await UserModel.ensureIndexes();
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('throws error if user not found', async () => {
    const args = { username: 'dummy username', password: 'dummy password' };
    await expect(login.resolve({ args })).rejects.toThrow('user not found');
  });

  it('throws error if password not correct', async () => {
    const password = faker.random.alphaNumeric(8);
    const user = await UserSeed({ password });

    const args = { username: user.username, password: 'dummy password' };
    await expect(login.resolve({ args })).rejects.toThrow('user not found');
  });

  it('returns token', async () => {
    const spy = jest.spyOn(LoginService, 'getToken');

    const password = faker.random.alphaNumeric(8);
    const user = await UserSeed({ password });

    const args = { username: user.username, password };
    const { token } = await login.resolve({ args });

    expect(spy).toBeCalled();
    expect(spy).toBeCalledWith(expect.objectContaining({ id: user.id }));
    expect(token).toBeString();

    spy.mockRestore();
  });
});

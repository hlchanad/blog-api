import { GraphQLError } from 'graphql';
import { schemaComposer } from 'graphql-compose';
import * as xor from 'xor-js';
import { UserModel } from '../../models';
import { UserTC } from '../types';

interface IArgs {
  id?: string;
  username?: string;
}

export const user = schemaComposer.createResolver<any, IArgs>({
  type: UserTC,
  name: 'user',
  kind: 'query',
  description: 'Get user by id or username',
  args: {
    id: 'String',
    username: 'String',
  },
  resolve: async ({ args: { id, username } }) => {
    if (!xor(id, username)) {
      throw new GraphQLError(
        'Exactly one of id or username should be provided',
      );
    }

    return UserModel.findOne({
      ...(id && { _id: id }),
      ...(username && { username }),
    });
  },
});

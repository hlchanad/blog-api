import { MongoInMemory } from '@test';
import * as mongoose from 'mongoose';
import { User } from '../../../models';
import { UserSeed } from '../../../models/user.seed';
import { user as userQuery } from '../user.query';

describe('resolver args', () => {
  it('should have optional arg `id`', async () => {
    expect(userQuery.getArgTypeName('id')).toBe('String');
  });

  it('should have optional arg `username`', async () => {
    expect(userQuery.getArgTypeName('username')).toBe('String');
  });
});

describe('resolver.resolve()', () => {
  let mongoServer: MongoInMemory.Server;
  let users: User[];

  beforeEach(async () => {
    mongoServer = await MongoInMemory.create();

    const dbUri = mongoServer.getConnectionString();

    await mongoose.connect(dbUri);

    users = await Promise.all([UserSeed(), UserSeed()]);
  });

  afterEach(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
  });

  it('rejects if no args provided', async () => {
    await expect(userQuery.resolve({ args: {} })).rejects.toThrow(
      'Exactly one of id or username should be provided',
    );
  });

  it('gets user by `id`', async () => {
    const user = await userQuery.resolve({ args: { id: users[1].id } });
    expect(user).toBeDefined();
    expect(user.id).toBe(users[1].id);
  });

  it('gets user by `username`', async () => {
    const user = await userQuery.resolve({
      args: { username: users[1].username },
    });
    expect(user).toBeDefined();
    expect(user.id).toBe(users[1].id);
  });
});

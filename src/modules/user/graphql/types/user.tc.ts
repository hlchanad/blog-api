import { schemaComposer } from 'graphql-compose';
import { getIdField, TimestampsTC } from '@modules/graphql';

export const ContactTC = schemaComposer.createObjectTC({
  name: 'UserContacts',
  fields: {
    type: 'String!',
    url: 'String!',
  },
});

export const ContactInputTC = schemaComposer.createInputTC({
  name: 'UserContactsInput',
  fields: {
    type: 'String!',
    url: 'String!',
  },
});

export const UserTC = schemaComposer.createObjectTC({
  name: 'User',
  interfaces: [TimestampsTC],
  fields: {
    id: getIdField(),
    username: 'String!',
    firstname: 'String!',
    lastname: 'String!',
    avatar: 'String',
    email: 'String!',
    contacts: ContactTC.NonNull.List.NonNull,
    description: 'String',
    createdAt: 'Date!',
    updatedAt: 'Date!',
  },
});

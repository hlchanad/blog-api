export const sns = {
  topics: {
    'post-created': {
      arn: process.env.SNS_ARN_POST_CREATED,
    },
    'post-updated': {
      arn: process.env.SNS_ARN_POST_UPDATED,
    },
  },
};

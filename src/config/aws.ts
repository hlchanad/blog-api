export const aws = {
  region: process.env.REGION,
  dynamodb: {
    'table-prefix': process.env.APP_NAME + '-',
  },
};

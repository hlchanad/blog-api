export const app = {
  name: process.env.APP_NAME,
  secret: process.env.SECRET ?? 'this is a secret',
};

import { database } from '@database';
import { parseAuthHeader } from '@modules/auth';
import { LoginService } from '@modules/user';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { ApolloServer } from 'apollo-server-express';
import * as express from 'express';
import { MongooseDataloaderFactory } from 'graphql-dataloader-mongoose';
import { schema } from './schema';
import { Context } from './types';

const server = new ApolloServer({
  schema,
  context: async ({ req }): Promise<Context> => ({
    user: await parseAuthHeader(
      LoginService.parseToken,
      req.header('authorization'),
    ),
    dataloader: new MongooseDataloaderFactory(),
  }),
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

(async () => {
  await database.mongo.connect();

  await server.start();

  const app = express();

  // Mount Apollo middleware here.
  server.applyMiddleware({ app, path: '/dev/graphql' });

  await new Promise<void>((resolve) =>
    app.listen({ port: 4000 }, () => resolve()),
  );

  console.info(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
})();

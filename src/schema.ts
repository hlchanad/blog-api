import { SchemaComposer } from 'graphql-compose';

import { PostMutation, PostQuery } from '@modules/post';
import { UserMutation, UserQuery } from '@modules/user';

const schemaComposer = new SchemaComposer();

schemaComposer.Query.addFields({
  ...PostQuery,
  ...UserQuery,
});

schemaComposer.Mutation.addFields({
  ...PostMutation,
  ...UserMutation,
});

export const schema = schemaComposer.buildSchema();

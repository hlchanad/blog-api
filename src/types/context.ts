import { User } from '@modules/user';
import { MongooseDataloaderFactory } from 'graphql-dataloader-mongoose';

export interface Context {
  user?: User;
  dataloader: MongooseDataloaderFactory;
}

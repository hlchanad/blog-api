import { MongoMemoryReplSet, MongoMemoryServer } from 'mongodb-memory-server';

export interface Server {
  getConnectionString: () => string;
  stop: () => Promise<boolean | undefined>;
}

export async function create({
  replica = false,
}: { replica?: boolean } = {}): Promise<Server> {
  const mongoServer = replica
    ? await MongoMemoryReplSet.create({
        replSet: { storageEngine: 'wiredTiger' },
        binary: { version: '4.4.8' },
      })
    : await MongoMemoryServer.create({
        binary: { version: '4.4.8' },
      });

  return {
    getConnectionString(): string {
      return mongoServer.getUri();
    },
    async stop(): Promise<boolean | undefined> {
      if (!mongoServer) return;
      return mongoServer.stop();
    },
  };
}
